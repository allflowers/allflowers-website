---
title: Allflowers présentent le clip de leur nouveau single "Hey! This Way"
date: 2024-03-02
---

Le duo de rock alternatif Allflowers nous montre le chemin de sortie de l’hiver dans le clip aux teintes printanières de leur nouveau single _Hey! This Way_.

La chanson est résolument rock, commence en douceur avec des accords de piano, puis progresse en intensité tout au long du morceau, en évitant soigneusement une structure habituelle couplet-refrain, avant d’exploser dans un final énergique au tempo doublé.

Le clip a été réalisé par Frenshyte. L’envie de collaborer avec Lara et Olivier est venue lorsqu’il se promenait par hasard à la BNF à Paris, et les a croisés, profitant du soleil et jouant de la musique. Il leur a alors proposé de réaliser un clip ensemble.

Le single _Hey! This Way_ sort sur les plateformes de streaming le 2 mars 2024 chez Verveine Records.

## Liens

- [Clip de Hey! This Way sur YouTube](https://www.youtube.com/watch?v=EYG3IJCXtrQ)
- [Avant-première en ligne le 02/03/2024 à 21:00 suivie d'un tchat](https://party.radiofreefedi.net)
- [Kit presse d'Allflowers]({{< param "pressKitFr" >}})

## Photos

Cliquez sur les photos pour une version en haute résolution.

{{< image "hey-this-way-thumbnail.jpg" "" "Photo de couverture" "yes" "medium" "Frenshyte Prod, Olivier Cléro" >}}

{{< image "hey-this-way-single.jpg" "" "Pochette du single Hey! This Way" "yes" "medium" >}}

{{< image "hey-this-way-snapshot-1.jpg" "" "Capture d'écran de la vidéo (1)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-2.jpg" "" "Capture d'écran de la vidéo (2)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-3.jpg" "" "Capture d'écran de la vidéo (3)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-4.jpg" "" "Capture d'écran de la vidéo (4)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-5.jpg" "" "Capture d'écran de la vidéo (5)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-6.jpg" "" "Capture d'écran de la vidéo (6)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-7.jpg" "" "Capture d'écran de la vidéo (7)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-8.jpg" "" "Capture d'écran de la vidéo (8)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-9.jpg" "" "Capture d'écran de la vidéo (9)" "yes" "medium" "Frenshyte Prod" >}}

{{< image "hey-this-way-snapshot-10.jpg" "" "Capture d'écran de la vidéo (10)" "yes" "medium" "Frenshyte Prod" >}}
