---
title: Aurélie
trackNumber: 6
---

_Aurélie_ is a composition by Lara (2018) for her childhood friend. It was originally named _Chanson pour Aurélie_, and rearranged in 2019.

It's a long piece that unfolds gradually, starting with an electric piano intro and ending with an almost grungy, Nirvana-inspired finale.

```txt
Ginger hair
Big blue eyes
Mischievous smile
That is you, for sure
See that is you, for sure
See that is you, for sure

See that is you (x3)

Don't be fooled
By the little child
Strong and wise
That is you, for sure
See that is you, for sure
See that is you, for sure

See that is you (x5)

Oooh
Oh-Oooh Oh-Oooh
(x4)

Oooh-aaah
(x2)

In your head
Fairies and magic
Hopes and dreams
See, your world is real (x3)

See your world (x3)

See, that is real (x3)
See, that is real, that is real
```
