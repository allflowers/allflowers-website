---
title: Somewhere Between the Lines
trackNumber: 3
---

{{< youtube yyVhakmMVbY >}}

This was originally an instrumental composition by Olivier (2012).

Lara was inspired by the instrumental and added lyrics and vocals. Olivier liked it so much that he totally re-recorded the instruments.

It became a pop-rock ballad with a dream-pop touch, reminiscent of the 90s bands such as The Cranberries.

```txt
[Verse]
(Em7 Cmaj7)
I’m waking up from a hazy dream
(G Gmaj9/F#)
Oh-oh such a long night
Foggy mind, I look around
For something I could recognize
What might have happened
I wonder, Can you tell me

[Pre-chorus]
I guess I lost myself
Very far away
I guess I lost myself
Can’t remember when

[Chorus]
(Emadd9 Cadd9)
If you know what place
(G Gmaj7/F#)
What place I am
Come and get me then
Please come back for me

[Break]
Mmmh…

[Verse]
I’m waking up from a hazy dream
Oh for such a long time
I walk around
Trying to figure out
What might have happened
I wonder
Can you tell me

[Pre-chorus]
I guess I lost myself
Very far away
I guess I lost myself
Can’t remember when

[Chorus]
If you know what place
What place I am
Come and get me then
Please come back for me

[Middle 8]
I hope I’m on my way
On my way
This Time
There seems to be a way
Somewhere between the lines
Ah ah-ah
Ah ah-ah
```
