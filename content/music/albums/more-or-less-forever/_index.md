---
title: More or Less Forever
date: 2021-11-11
type: lists
layout: album
cover: cover.png
label: Verveine Records
labelUrl: /verveine-records
releaseType: LP
released: true
platforms:
  - name: spotify
    url: https://open.spotify.com/album/7rHmwEiLXEw43NxRjUFL3l
  - name: deezer
    url: https://www.deezer.com/album/274880552
  - name: appleMusic
    url: https://music.apple.com/album/more-or-less-forever/1596769921
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/more-or-less-forever
  - name: soundcloud
    url: https://soundcloud.com/allflowers/sets/more-or-less-forever
  - name: youtube
    url: https://www.youtube.com/watch?v=aEt7Ff8xsgQ&list=PL5Z4OiO-L5_9_nYqJ3-_wFMszYXuscSRg
orderCD: https://allflowers.bandcamp.com/album/more-or-less-forever
quotes:
  - note: 4.5
    date: 2022-10-22
    author: La Sélection AlbumRock, Contre-victoires de la Musique winners
    text: Des paroles intimistes, teintées de spleen adolescent et de pull-overs trop grands.
    url: https://www.albumrock.net/selection-avis-allflowers-966.html
  - note: 5
    date: 2022-02-08
    author: RSTLSS, La Nouveauté rock française
    text: Cet album est culte avant l’heure, prédestiné à un très bel avenir.
    url: https://rstlss.com/podcast/la-nouveaute-rock-francaise-56-allflowers-zagred-song/
  - note: 5
    date: 2022-09-24
    author: Stone Rock Radio
    text: Je suis littérallement tombé amoureux de la voix de la chanteuse, c’est juste magnifique !
    url: /passage-radio-stone-rock-radio.mp3
  - note: 5
    date: 2022-02-02
    author: Radio Free Fedi
    text: They present an outstanding individual and modern sound with unmistakable nods to alternative's past (Siouxsie, The Cure, Lush and everything delightfully alt).
    url: https://musician.social/@radiofreefedi/109797069806360571
---

## About

Composed and recorded over the course of 3 years, Allflowers' first album is a vibrant hommage to the Alternative Rock of the 90's, and specifically UK bands fronted by female singers: [The Cranberries](https://www.youtube.com/watch?v=Yam5uK6e-bQ), [The Sundays](https://www.youtube.com/watch?v=FHsip5xOenQ), [Suzanne Vega](https://www.youtube.com/watch?v=0df0racc3vk) and [Cocteau Twins](https://www.youtube.com/watch?v=6KnYw4EwYGc). The tracks are also reminiscent of Grunge-era US bands like [Smashing Pumpkins](https://www.youtube.com/watch?v=NOG3eus4ZSo), [Soundgarden](https://www.youtube.com/watch?v=3mbBbFH9fAg) or [Nirvana](https://www.youtube.com/watch?v=vabnZ9-ex7o).

Lara and Olivier both had their own compositions and helped each other to bring the best of them by complimenting the demos with vocals or instrumentation. They collaborated remotely, mostly during the Covid lockdown, sending each other audio files and musical ideas. Production, mixing and mastering was entirely done in Olivier's home-studio.

## Credits

All songs composed, written and arranged by **Lara Martinovic** and **Olivier Cléro**, and published by **Verveine Records**. Recorded, mixed and mastered in Paris, France, at **Verveine Studios** between November 2018 and November 2021.

- **Lara Martinovic:** Vocals, Keyboard, Clarinet
- **Olivier Cléro:** Guitar, Bass, Drums.

Special thanks to our amazing musician friends:

- **Catherine Jacob:** Violin.
- **Barbara Rialland:** Cello.
- **Louis Couka:** Fender Rhodes.

Picture credits:

- **[Emilie Vernerey](https://www.instagram.com/emilie.vernerey):** Cover picture.
- **[Judith Wahler](https://www.instagram.com/juwahnji/):** Other pictures.
