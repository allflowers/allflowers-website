---
title: Tired of Pretending
date: 2023-07-22
type: lists
layout: album
cover: cover.jpg
label: Verveine Records
labelUrl: /verveine-records
releaseType: single
released: true
platforms:
  - name: spotify
    url: https://open.spotify.com/album/61yuTl5yixIg2Fv4cCPzQ8
  - name: deezer
    url: https://www.deezer.com/album/458275845
  - name: appleMusic
    url: https://music.apple.com/album/tired-of-pretending-single/1694528039
  - name: bandcamp
    url: https://allflowers.bandcamp.com/album/tired-of-pretending
  - name: youtube
    url: https://www.youtube.com/watch?v=cXBs700IBog
tracklist:
  - title: Tired of Pretending
    url: /music/albums/more-or-less-forever/tired-of-pretending
---

## Credits

Composed, written and arranged by **Lara Martinovic** and **Olivier Cléro**, and published by **Verveine Records**. Recorded, mixed and mastered in Paris, France, at **Verveine Studios** between November 2018 and November 2021.

- **Lara Martinovic:** Vocals.
- **Olivier Cléro:** Guitar, Organ, Bass, Drums.

Picture credits:

- **Nikita Blauwart:** Cover picture.
- **Olivier Cléro:** Cover design.
