---
title: Verveine Records
---

{{< image "/img/verveine_records_logo.svg" "" "Verveine Records logo" "no" "small" >}}

## An Independant Label

Verveine Records is the independant label we created to promote our music and the music of our friends.

Its is a French **nonprofit organization**, ruled by the law of the 1st of July 1901 (according to French law), created in March 2023.

The goal of this organization is music promotion by the means of events organization, music production and artist management.

{{< label-email-button >}}

## Donations

Volunteering is the rule concerning the organization: all donations and receipts are kept and used by the legal person, i.e. the organization, without any distribution to the directors or the members, despite their work and their investment.

All donations et benefits will go directly to the organization, and will be reinvested into the project.

## Legal Information

### Situation

Adresse: `Verveine Records, 18 rue Roger-Henri Guerrand, 35000 Rennes, France`

SIREN: `923212971`

RNA: `W751268816`

### Board

| Name            | Role                     |
| :-------------- | :----------------------- |
| Olivier Cléro   | Président                |
| Lara Martinovic | Trésorière et secrétaire |

## Documents

| Date       | Document                                                                                               |
| ---------- | :----------------------------------------------------------------------------------------------------- |
| 06/08/2024 | [Statuts de l'association](/verveine-records/statuts-association-verveine-records.pdf)                 |
| 02/03/2023 | [Procès verbal - Assemblée générale constitutive](/verveine-records/pv-2023-03-02-ag-constitutive.pdf) |
| 07/03/2023 | [Parution au Journal officiel](/verveine-records/parution-journal-officiel-verveine-records.pdf)       |
| 15/03/2023 | [Avis de situation au répertoire SIRENE](/verveine-records/avis-situation-sirene-verveine-records.pdf) |
